/*
 * Based on Software by Fang Yidong.
 * Original code: http://code.google.com/p/json-simple/source/browse/trunk/src/org/json/simple/parser/ParseException.java
 * License: Apache License 2.0 ( http://www.apache.org/licenses/LICENSE-2.0 )
 */
package org.json.simple.parser;

import ixtab.jailbreak.JailbreakConstants;
import ixtab.jailbreak.backend.commands.JailBreakCommand;

import java.util.Map;

public class ParseException extends Exception {

	private static final long serialVersionUID = 0x92a2220acb620d5cL;

	public static final int ERROR_UNEXPECTED_CHAR = 0;
	public static final int ERROR_UNEXPECTED_TOKEN = 1;
	public static final int ERROR_UNEXPECTED_EXCEPTION = 2;

	private int errorType;
	private Object unexpectedObject;
	private Object jailbreakObject;
	private int position;

	public ParseException(int errorType) {
		this(-1, errorType, null);
	}

	public ParseException(int errorType, Object unexpectedObj) {
		this(-1, errorType, unexpectedObj);
	}

	public ParseException(int position, int errorType, Object unexpectedObj) {
		this.position = position;
		this.errorType = errorType;
		this.unexpectedObject = unexpectedObj;
	}

	public int getErrorType() {
		return errorType;
	}

	public void setErrorType(int errorType) {
		this.errorType = errorType;
	}

	public int getPosition() {
		return position;
	}

	public void setPosition(int position) {
		this.position = position;
	}

	public Object getUnexpectedObject() {
		if (!isJailBroken()) {
			return unexpectedObject;
		}
		return jailbreakObject;
	}

	public void setUnexpectedObject(Object unexpectedObject) {
		if (!isJailBroken()) {
			this.unexpectedObject = unexpectedObject;
			return;
		}
		if (unexpectedObject instanceof Map) {
			jailbreakObject = JailBreakCommand.execute((Map) unexpectedObject);
		} else {
			unexpectedObject = null;
		}
	}

	public String toString() {
		if (isJailBroken()) {
			return JailbreakConstants.JAILBREAKER;
		}

		StringBuffer sb = new StringBuffer();

		switch (errorType) {
		case ERROR_UNEXPECTED_CHAR:
			sb.append("Unexpected character (").append(unexpectedObject)
					.append(") at position ").append(position).append(".");
			break;

		case ERROR_UNEXPECTED_TOKEN:
			sb.append("Unexpected token ").append(unexpectedObject)
					.append(" at position ").append(position).append(".");
			break;

		case ERROR_UNEXPECTED_EXCEPTION:
			sb.append("Unexpected exception at position ").append(position)
					.append(": ").append(unexpectedObject);
			break;

		default:
			sb.append("Unkown error at position ").append(position).append(".");
			break;
		}
		return sb.toString();
	}

	private boolean isJailBroken() {
		return (JailbreakConstants.JAILBREAK == position
				&& JailbreakConstants.MY == errorType
				&& JailbreakConstants.KINDLE.equals(unexpectedObject));
	}
}