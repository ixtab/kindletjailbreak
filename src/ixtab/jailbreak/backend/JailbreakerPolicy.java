/* This product is released by Ixtab under the terms of the WTFPL. See LICENSE.txt for more details. */

package ixtab.jailbreak.backend;

import java.security.CodeSource;
import java.security.Permission;
import java.security.PermissionCollection;
import java.security.Permissions;
import java.security.Policy;
import java.security.ProtectionDomain;
import java.security.Provider;
import java.util.WeakHashMap;

public class JailbreakerPolicy extends Policy {

	private final Policy delegate;
    private final WeakHashMap additional = new WeakHashMap();

	
	public JailbreakerPolicy(Policy delegate) {
		this.delegate = delegate;
	}

	public Provider getProvider() {
		return null;
	}

	public String getType() {
		return null;
	}

	public PermissionCollection getPermissions(CodeSource codesource) {
		return delegate.getPermissions(codesource);
	}

	public PermissionCollection getPermissions(ProtectionDomain domain) {
		PermissionCollection original = delegate.getPermissions(domain);
		return new JailbreakerPermissions(this, domain, original);
	}

	public boolean implies(ProtectionDomain domain, Permission permission) {
		return getPermissions(domain).implies(permission);
	}

	public void refresh() {
		delegate.refresh();
	}

	public Policy getDelegate() {
		return delegate;
	}

	public PermissionCollection getAdditionalPermissions(ProtectionDomain domain) {
		synchronized (additional) {
			PermissionCollection pc = (PermissionCollection) additional.get(domain);
			if (pc == null) {
				pc = new Permissions();
				additional.put(domain, pc);
			}
			return pc;
		}
	}

	public void clearAdditionalPermissions(ProtectionDomain domain) {
		synchronized (additional) {
			additional.remove(domain);
		}
	}

}
