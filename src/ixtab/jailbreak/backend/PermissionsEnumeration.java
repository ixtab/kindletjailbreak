/* This product is released by Ixtab under the terms of the WTFPL. See LICENSE.txt for more details. */

package ixtab.jailbreak.backend;

import java.util.Enumeration;


public class PermissionsEnumeration implements Enumeration {

	private final Enumeration[] enums;
	private int currentIndex = 0;
	
	public PermissionsEnumeration(Enumeration[] enums) {
		super();
		this.enums = new Enumeration[enums == null ? 0 : enums.length];
		for (int i = 0; i < this.enums.length; ++i) {
			this.enums[i] = enums[i];
		}
	}

	public boolean hasMoreElements() {
		if (currentIndex >= enums.length) {
			return false;
		}
		if (enums[currentIndex].hasMoreElements()) {
			return true;
		}
		++currentIndex;
		return hasMoreElements();
	}

	public Object nextElement() {
		if (!hasMoreElements()) return null;
		return enums[currentIndex].nextElement();
	}

}
