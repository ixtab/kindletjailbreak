/* This product is released by Ixtab under the terms of the WTFPL. See LICENSE.txt for more details. */

package ixtab.jailbreak.backend.commands;

import ixtab.jailbreak.JailbreakConstants;
import ixtab.jailbreak.backend.JailbreakerPolicy;

import java.security.Policy;
import java.util.Map;


public class PolicyCommand extends JailBreakCommand implements JailbreakConstants.Protocol {

	public static final String TYPE = POLICY;

	protected Map run(Map request, Map response) {
		Policy policy = Policy.getPolicy();
		Boolean replace = (Boolean) request.get(POLICY_REPLACE_WITH);
		boolean set = false;
		
		if (policy != null && replace != null) {
			if (replace.booleanValue()) {
				if (!(policy instanceof JailbreakerPolicy)) {
					policy = new JailbreakerPolicy(policy);
					set = true;
				}
			} else if (policy instanceof JailbreakerPolicy) {
				policy = ((JailbreakerPolicy)policy).getDelegate();
				set = true;
			}
		}
		
		if (set) {
			Policy.setPolicy(policy);
		}

		response.put(TYPE, policy);
		return response;
	}

	protected boolean needsPrivileges() {
		return true;
	}
	
}
