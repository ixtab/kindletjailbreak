/* This product is released by Ixtab under the terms of the WTFPL. See LICENSE.txt for more details. */

package ixtab.jailbreak.backend.commands;

import ixtab.jailbreak.JailbreakConstants;

import java.security.ProtectionDomain;
import java.util.Map;


public class ProtectionDomainCommand extends JailBreakCommand implements JailbreakConstants.Protocol {

	public static final String TYPE = PROTECTION_DOMAIN;

	protected Map run(Map request, Map response) {
		Class clazz = (Class) request.get(PROTECTION_DOMAIN_CLASS);
		ProtectionDomain result = clazz.getProtectionDomain();
		response.put(TYPE, result);
		return response;
	}

	protected boolean needsPrivileges() {
		return true;
	}
	
}
