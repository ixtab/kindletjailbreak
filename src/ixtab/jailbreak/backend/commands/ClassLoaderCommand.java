/* This product is released by Ixtab under the terms of the WTFPL. See LICENSE.txt for more details. */

package ixtab.jailbreak.backend.commands;

import ixtab.jailbreak.JailbreakConstants;
import ixtab.jailbreak.backend.JailbreakerClassLoader;

import java.net.URLClassLoader;
import java.util.Map;

public class ClassLoaderCommand extends JailBreakCommand {

        public static final String TYPE = JailbreakConstants.Protocol.CLASSLOADER;

        protected Map run(Map request, Map response) {
        	
                URLClassLoader unjailed = null;
                Object parent = request.get(JailbreakConstants.Protocol.CLASSLOADER_PARENT);
                
                if (parent != null && parent instanceof URLClassLoader) {
                	if (parent instanceof JailbreakerClassLoader) {
                		unjailed = (URLClassLoader) parent;
                	} else {
                		unjailed = new JailbreakerClassLoader((URLClassLoader)parent);
                	}
                }
                
                response.put(TYPE, unjailed);
                return response;
        }

        protected boolean needsPrivileges() {
                return true;
        }
        
}
