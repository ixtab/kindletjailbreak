/* This product is released by Ixtab under the terms of the WTFPL. See LICENSE.txt for more details. */

package ixtab.jailbreak.backend.commands;

import ixtab.jailbreak.JailbreakConstants;

import java.util.Map;

public class VersionCommand extends JailBreakCommand {

	public static final String TYPE = JailbreakConstants.Protocol.VERSION;
	private static final Integer VERSION = new Integer(JailbreakConstants.VERSION);

	protected Map run(Map request, Map response) {
		response.put(TYPE, VERSION);
		return response;
	}

	protected boolean needsPrivileges() {
		return false;
	}
	
}
