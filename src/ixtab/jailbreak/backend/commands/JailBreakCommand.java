/* This product is released by Ixtab under the terms of the WTFPL. See LICENSE.txt for more details. */

package ixtab.jailbreak.backend.commands;

import ixtab.jailbreak.JailbreakConstants;

import java.security.AccessController;
import java.security.PrivilegedExceptionAction;
import java.util.HashMap;
import java.util.Map;

public abstract class JailBreakCommand {

	public static final String KEY_REQUESTTYPE = JailbreakConstants.Protocol.COMMAND;
	
	public static Map execute(Object input) {
		
		final Map request = verifyMap(input);
		String type = verifyType(request);
		
		final JailBreakCommand runner = CommandFactory.find(type);
		try {
			final Map response = new HashMap();
			if (runner.needsPrivileges()) {
				return (Map) AccessController.doPrivileged(new PrivilegedExceptionAction() {
					public Object run() {
						return runner.doRun(request, response);
					}
				});
			} else {
				return runner.run(request, response);
			}
		} catch (Throwable t) {
			throw new RuntimeException(t);
		}
	}

	private static Map verifyMap(Object input) {
		if (input == null) {
			throw new RuntimeException("NULL request");
		}
		if (!(input instanceof Map)) {
			throw new RuntimeException("request must be a java.util.Map");
		}
		return (Map) input;
	}

	private static String verifyType(final Map request) {
		Object type = request.get(KEY_REQUESTTYPE);
		if (type == null || !(type instanceof String)) {
			throw new RuntimeException("NULL or non-String request type");
		}
		return (String)type;
	}

	protected abstract Map run(Map request, Map response) throws Throwable;
	
	private final Map doRun(Map request, Map response) {
		try {
			return run(request, response);
		} catch (Throwable e) {
			throw new RuntimeException(e);
		}
	}
	protected abstract boolean needsPrivileges();

}
