/* This product is released by Ixtab under the terms of the WTFPL. See LICENSE.txt for more details. */

package ixtab.jailbreak.backend.commands;

public class CommandFactory {

	public static JailBreakCommand find(String type) {
		if (VersionCommand.TYPE.equals(type)) return new VersionCommand();
		if (ProtectionDomainCommand.TYPE.equals(type)) return new ProtectionDomainCommand();
		if (PolicyCommand.TYPE.equals(type)) return new PolicyCommand();
		if (ClassLoaderCommand.TYPE.equals(type)) return new ClassLoaderCommand();
		throw new RuntimeException("No implementation found for command "+ type);
	}

}
