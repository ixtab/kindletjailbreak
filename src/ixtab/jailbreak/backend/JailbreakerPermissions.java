/* This product is released by Ixtab under the terms of the WTFPL. See LICENSE.txt for more details. */

package ixtab.jailbreak.backend;

import java.security.Permission;
import java.security.PermissionCollection;
import java.security.ProtectionDomain;
import java.util.Enumeration;

class JailbreakerPermissions extends PermissionCollection {

	private static final long serialVersionUID = 249538019500125910L;
	
	private PermissionCollection readable;
	private final JailbreakerPolicy parent;
	private final ProtectionDomain domain;
	
	public JailbreakerPermissions(JailbreakerPolicy parent, ProtectionDomain domain, PermissionCollection delegate) {
		super();
		this.parent = parent;
		this.domain = domain;
		this.readable = delegate;
	}

	private PermissionCollection writable() {
		return parent.getAdditionalPermissions(domain);
	}
	
	public void add(Permission permission) {
		PermissionCollection writable = writable();
		if (!writable.implies(permission)) { writable().add(permission); }
	}

	public boolean implies(Permission permission) {
		return writable().implies(permission) || readable.implies(permission);
	}

	public Enumeration elements() {
		return new PermissionsEnumeration(new Enumeration[]{readable.elements(), writable().elements()});
	}

	public void setReadOnly() {
		parent.clearAdditionalPermissions(domain);
	}

	public boolean isReadOnly() {
		return !writable().elements().hasMoreElements();
	}

}
