/* This product is released by Ixtab under the terms of the WTFPL. See LICENSE.txt for more details. */

package ixtab.jailbreak.backend;

import java.io.IOException;
import java.lang.reflect.Method;
import java.net.URL;
import java.net.URLClassLoader;
import java.security.AllPermission;
import java.security.CodeSource;
import java.security.PermissionCollection;
import java.security.Permissions;
import java.util.Enumeration;

public class JailbreakerClassLoader extends URLClassLoader {

	private final ClassLoader outOfJail;

	public JailbreakerClassLoader(URLClassLoader jailed) {
		super(jailed.getURLs(), jailed);
		outOfJail = jailed.getParent();
	}

	protected Class findClass(String name) throws ClassNotFoundException {
		try {
			return super.findClass(name);
		} catch (ClassNotFoundException e) {
			Class r = outOfJail.loadClass(name);
			if (r != null)
				return r;
			throw e;
		}
	}

	public URL findResource(String name) {
		URL url = super.findResource(name);
		if (url != null)
			return url;
		return outOfJail.getResource(name);
	}

	public Enumeration findResources(String name) throws IOException {
		Enumeration result = super.findResources(name);
		if (result.hasMoreElements())
			return result;
		return outOfJail.getResources(name);
	}

	protected synchronized Class loadClass(String name, boolean resolve)
			throws ClassNotFoundException {
		Class c = findLoadedClass(name);
		if (c == null) {
			try {
				c = outOfJail.loadClass(name);
			} catch (ClassNotFoundException e) {
			}
			if (c == null) {
				c = findClass(name);
			}
		}
		if (resolve) {
			resolveClass(c);
		}
		return c;
	}

	protected PermissionCollection getPermissions(CodeSource codesource) {
		try {
			Method m = URLClassLoader.class.getDeclaredMethod("getPermissions", new Class[] {CodeSource.class});
			m.setAccessible(true);
			return (PermissionCollection) m.invoke(getParent(), new Object[] {codesource});
		} catch (Throwable t) {
			/* it should never go wrong. If it still does, we essentially fall back to a jailbroken system. */
			Permissions all = new Permissions();
			all.add(new AllPermission());
			return all;
		}
	}
	
	

}
